package cl.lherrera.librerias.fechas;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;

/**
 *
 * @author luisherrera 2019
 * @version 19.0.0
 */
public class ServicioDeFechas {
    /**
     * Entero que retorna los resultados en mayúsculas.
     * Para cualquier sentencia opcional
     * irá este nombre para una
     * mayor claridad.
     */
    public static final int UPPER_CASE = 0;
    
    /**
     * Entero que retorna los resultados en minúsculas.
     * Para cualquier sentencia opcional
     * irá este nombre para una
     * mayor claridad.
     */
    public static final int LOWERCASE = 1;
    
    /**
     * Entero que retorna los resultados con su primer caracter en mayúscula
     * Para cualquier sentencia opcional
     * irá este nombre para una
     * mayor claridad.
     */
    public static final int INIT_CAP = 2;

    /**
     * ISO 8601 aborda la incertidumbre al establecer una forma acordada 
     * internacionalmente para representar fechas: <i>yyyy-MM-dd</i>.
     * 
     * EJ: {@code 2020-02-01}, esto es el primero de febrero
     * del año 2020.
     * 
     */
    public static final String ISO_8601_FORMAT = "yyyy-MM-dd";
    
    /**
     * Un dia, posee 86400000 milisegundos, esta constante es usada debido a
     * que las fechas del tipo {@link java.util.Date}, 
     * están representadas en milisegundos.
     * 
     * @see java.util.Date#getTime()
     */
    public static final long MILIS_EN_UN_DIA = 86400000;
    
    /**
     * Retorna  un <code>Date</code>, con los días agregados.
     * @param fecha <i>Date</i> la fecha a la que se le sumarán días.
     * @param dias  <i>int</i> entero con los días a adicionar.
     * @return Date
     */
    public static Date agregaDias(Date fecha, int dias){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);

        calendar.add(Calendar.DAY_OF_YEAR, dias);

        return calendar.getTime();
    }
    
    /**
     * Retorna  un <code>Date</code>, con los días agregados.
     * Se puede usar {@link #agregaDias(java.util.Date, int) agregaDias}
     * con los días en negativo para el mismo efecto
     * 
     * @param fecha <i>Date</i> la fecha a la que se le sumarán días.
     * @param dias  <i>int</i> entero con los días a adicionar.
     * @return {@code Date} la fecha con los días restados.
     */
    public static Date restarDias(Date fecha, int dias){
        dias = dias * (-1);

        return agregaDias(fecha, dias);
    }
    
    /**
     * Agrega meses a una fecha dada.
     * 
     * @param fecha {@code Date} fecha a la que se le sumarán los meses.
     * @param numMeses {@code int} numeros de meses a sumar
     * @return {@code Date} fecha con los meses sumados.
     */
    public static Date agregarMeses(Date fecha, int numMeses) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        
        calendar.add(Calendar.MONTH, numMeses);
        
        return calendar.getTime();
    }
    
    /**
     * Resta meses a una fecha dada.
     * 
     * @param fecha {@code Date} fecha a la que se le restarán los meses.
     * @param numMeses {@code int} numeros de meses a restar.
     * @return {@code Date} fecha con los meses restados.
     */
    public static Date restarMeses(Date fecha, int numMeses) {
        return agregarMeses(fecha, numMeses * (-1));
    }
    
    /**
     * Toma una fecha {@link  java.util.Date} y la transforma en texto, según 
     * el formáto que se le indique.
     * <p>
     *  <code>
     *     deFechaACadenaConFormato(new Date(), "dd/MM/yyyy");
     * </code>
     * </p>
     * @param fecha {@code Date} fecha a transformar.
     * @param formato {@code String} patrón, ej: {@literal "dd/MM/yy"}.
     * @return {@code String} texto de la fecha Ej: {@code "01/04/2025"}. 
     */
    public static String deFechaACadenaConFormato(Date fecha, String formato){
        SimpleDateFormat formatoFecha;
        formatoFecha = new SimpleDateFormat(formato);

        return formatoFecha.format(fecha);
    }

    /**
     * Retorna la diferencia en <i>días</i> entre dos fechas.
     * 
     * @param primera {@code Date} 
     * @param segunda {@code Date} 
     * @return {@code int} diferencia en días.
     */
    public static int ObtenerDifDias(Date primera, Date segunda){
        long diferencia = primera.getTime() - segunda.getTime();
        
        diferencia = (diferencia < 0)? diferencia * (-1): diferencia;
        
        return (int) (diferencia / MILIS_EN_UN_DIA);
    }
    
    /**
     * Recibe una fecha y un formato {@link #LOWERCASE} o {@link #UPPER_CASE}
     * y retorna el día de la semana.
     * 
     * @param fecha {@link java.util.Date} fecha a extraer el nombre del día.
     * @param formato {@link #LOWERCASE} o {@link #UPPER_CASE}
     * @return retorno un {@link java.lang.String} con el nómbre
     */
    public static String obtenerNomDiaSemana(Date fecha, int formato){
        String retorno = "";
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        
        switch(calendar.get(Calendar.DAY_OF_WEEK)){
            case Calendar.MONDAY:
                retorno = "Lunes";
                break;
            case Calendar.TUESDAY:
                retorno = "Martes";
                break;
            case Calendar.WEDNESDAY:
                retorno = "Miércoles";
                break;
            case Calendar.THURSDAY:
                retorno = "Jueves";
                break;
            case Calendar.FRIDAY:
                retorno = "Viernes";
                break;
            case Calendar.SATURDAY:
                retorno = "Sabado";
                break;
            case Calendar.SUNDAY:
                retorno = "Domingo";
                break;
        }
        
        return obtenerConFormato(retorno, formato);
    }
    
    /**
     * Recibe una fecha y un formato {@link #LOWERCASE} o {@link #UPPER_CASE}
     * y retorna el nombre del mes.
     * 
     * @param fecha {@link java.util.Date} fecha a extraer el nombre del día.
     * @param formato {@link #LOWERCASE}, {@link #UPPER_CASE} y 
     * {@link #INIT_CAP}
     * @return retorno un {@link java.lang.String} con el nómbre
     */
    public static String ObtenerNomMes(Date fecha, int formato){
        String retorno = "";

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);

        switch(calendar.get(Calendar.MONTH)){
            case Calendar.JANUARY:
                retorno = "Enero";
                break;
            case Calendar.FEBRUARY:
                retorno = "Febrero";
                break;
            case Calendar.MARCH:
                retorno = "Marzo";
                break;
            case Calendar.APRIL:
                retorno = "Abril";
                break;
            case Calendar.MAY:
                retorno = "Mayo";
                break;
            case Calendar.JUNE:
                retorno = "Junio";
                break;
            case Calendar.JULY:
                retorno = "Julio";
                break;
            case Calendar.AUGUST:
                retorno = "Agosto";
                break;
            case Calendar.SEPTEMBER:
                retorno = "Agosto";
                break;
            case Calendar.OCTOBER:
                retorno = "Octubre";
                break;
            case Calendar.NOVEMBER:
                retorno = "Noviembre";
                break;
            case Calendar.DECEMBER:
                retorno = "Noviembre";
                break;
        }

        return obtenerConFormato(retorno, formato);
    }

    /**
     * Formatea un string al formáto solicitado.
     * 
     * @param formato {@link #LOWERCASE}, {@link #UPPER_CASE} y 
     * {@link #INIT_CAP}
     * @return {@link java.lang.String} con el formáto que se le pase.
     * @param str cadena a formatear 
     */
    private static String obtenerConFormato(String str, int formato){
        String retorno = "";

        switch(formato){
            case UPPER_CASE:
                retorno = str.toUpperCase();
                break;
            case LOWERCASE:
                retorno = str.toLowerCase();
                break;
            case INIT_CAP:
                retorno = str;
                break;
        }

        return retorno;
    }
    
    /**
     * Este método entrega la fecha detallada en texto, pasando una fecha 
     * {@link java.lang.String} y un formáto.
     * 
     * @param fecha {@link java.util.Date} fecha a extraer el nombre del día.
     * @param formato {@link #LOWERCASE}, {@link #UPPER_CASE} y 
     * {@link #INIT_CAP}
     * @return {@link java.lang.String} ej: {@code Lunes 06 de Julio del 2020}
     */
    public static String obtenerTextoFechaDetallada(Date fecha, int formato){
        String retorno = "%s %s de %s del %s";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        
        String nomDia = obtenerNomDiaSemana(fecha, formato);
        
        int numDia = calendar.get(Calendar.DAY_OF_MONTH);

        String sNumDia = String.format("%02d", numDia); // ceros a dos dígitos
        
        String mes = ObtenerNomMes(fecha, formato);
        
        String anio = Integer.toString(calendar.get(Calendar.YEAR));
        
        retorno = String.format(retorno, nomDia, sNumDia, mes, anio);
        
        return obtenerConFormato(retorno, formato);
    }
    
    public static void main(String... args){
        Date date = new Date();
        
        date = agregaDias(date, 4);
        
        System.out.println(obtenerTextoFechaDetallada(date, UPPER_CASE));
    }
    
}
