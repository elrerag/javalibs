package cl.lherrera.librerias.fechas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import junit.framework.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ServicioDeFechasTest {

    Date fecha;
    Date fecha2;

    @Test
    public void vemosSiPodemosAgregarFechas() {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
            fecha = formatoFecha.parse("01/01/2019");
            fecha2 = formatoFecha.parse("03/01/2019");

            fecha = ServicioDeFechas.agregaDias(fecha, 2);

            assertEquals(fecha, fecha2);

        } catch (ParseException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void vemosSiPodemosRestarFechas() {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
            fecha = formatoFecha.parse("31-12-2018");
            fecha2 = ServicioDeFechas.restarDias(
                    formatoFecha.parse("02-01-2019"),
                    2
            );
            assertEquals(fecha, fecha2);
        } catch (ParseException ex) {
            Assert.fail(ex.getMessage());
        }

    }

    @Test
    public void vemosSiPodemosAgregarMeses() {
        SimpleDateFormat formatoFecha = new SimpleDateFormat(
                ServicioDeFechas.ISO_8601_FORMAT
        );

        try {
            fecha = formatoFecha.parse("2025-01-01");
            fecha2 = formatoFecha.parse("2025-12-01");

            fecha = ServicioDeFechas.agregarMeses(fecha, 11);

            assertEquals(fecha, fecha2);

        } catch (ParseException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void vemosSiPodemosQuitarMeses() {
        SimpleDateFormat formatoFecha = new SimpleDateFormat(
                ServicioDeFechas.ISO_8601_FORMAT
        );

        try {
            fecha = formatoFecha.parse("2025-01-01");
            fecha2 = formatoFecha.parse("2025-12-01");

            fecha2 = ServicioDeFechas.restarMeses(fecha2, 11);

            assertEquals(fecha, fecha2);

        } catch (ParseException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void vemosSiObtenemosUnStringDeUnaFecha() {
        try {
            String miFomato = ServicioDeFechas.ISO_8601_FORMAT;
            String fechaTxt = "2020-03-26";

            SimpleDateFormat formatoFecha = new SimpleDateFormat(miFomato);
            
            fecha = formatoFecha.parse(fechaTxt);

            String formatoDePrueba = ServicioDeFechas.deFechaACadenaConFormato(
                    fecha,
                    miFomato
            );
            
            assertEquals(fechaTxt, formatoDePrueba);

        } catch (ParseException ex) {
            Assert.fail(ex.getMessage());
        }

    }
    
    @Test
    public void probamosSiObtenemosLaDiferenciaDeDiasEntreDosFechas(){
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            fecha = formatoFecha.parse("2019-12-31");
            fecha2 = formatoFecha.parse("2020-01-01");

            assertEquals(ServicioDeFechas.ObtenerDifDias(fecha, fecha2), 1);
            
        } catch (ParseException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void probarSiObtenemosElNombreDelDiaDeLaSemanaDeUnaFecha(){
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            fecha = formatoFecha.parse("2019-04-05");
            String dia = ServicioDeFechas.obtenerNomDiaSemana(
                    fecha, 
                    ServicioDeFechas.UPPER_CASE
            );
            assertEquals("VIERNES", dia);
            dia = ServicioDeFechas.obtenerNomDiaSemana(
                    fecha, 
                    ServicioDeFechas.LOWERCASE
            );
            assertEquals("viernes", dia);
        } catch (ParseException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void probarSiObtenemosElNombreDelMesDeUnaFecha(){
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            fecha = formatoFecha.parse("2019-02-20");

            String mesPrueba = "Febrero";
            String mes = ServicioDeFechas.ObtenerNomMes(
                    fecha, 
                    ServicioDeFechas.INIT_CAP
            );
            
            assertEquals(mes, mesPrueba);

        } catch (ParseException ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void probarSiObtenemosUnaFechaDetallada(){
        try {
            SimpleDateFormat formato = new SimpleDateFormat(
                    ServicioDeFechas.ISO_8601_FORMAT
            );
            fecha = formato.parse("2020-07-06");
            
            String fechaDetallada = "Lunes 06 de Julio del 2020";
            
            String fechaDePrueba = ServicioDeFechas.obtenerTextoFechaDetallada(
                    fecha,
                    ServicioDeFechas.INIT_CAP
            );

            assertEquals(fechaDetallada, fechaDePrueba);
        } catch (ParseException ex) {
            Assert.fail(ex.getMessage());
        }
    }

}
